
Romeo just entered
               Romeo on stage
              Juliet

Juliet just entered
               Romeo on stage
              Juliet on stage

Computing int_sub(64, 1) = 63

Romeo's previous value was 0.
Romeo's new value is 63.

Pushing 63 onto Juliet
Juliet's stack is now: 63

Computing int_add(16, 32) = 48

Computing int_add(1, 48) = 49

Romeo's previous value was 63.
Romeo's new value is 49.

Pushing 49 onto Juliet
Juliet's stack is now: 49 63

Computing int_add(2, 32) = 34

Computing int_add(1, 2) = 3

Computing int_mul(3, 34) = 102

Romeo's previous value was 49.
Romeo's new value is 102.

Pushing 102 onto Juliet
Juliet's stack is now: 102 49 63

Computing int_add(32, 64) = 96

Computing int_add(2, 96) = 98

Romeo's previous value was 102.
Romeo's new value is 98.

Pushing 98 onto Juliet
Juliet's stack is now: 98 102 49 63

Computing int_add(1, 8) = 9

Computing int_sub(64, 9) = 55

Romeo's previous value was 98.
Romeo's new value is 55.

Pushing 55 onto Juliet
Juliet's stack is now: 55 98 102 49 63

Computing int_sub(128, 4) = 124

Romeo's previous value was 55.
Romeo's new value is 124.

Pushing 124 onto Juliet
Juliet's stack is now: 124 55 98 102 49 63

Computing int_add(32, 64) = 96

Computing int_add(1, 4) = 5

Computing int_add(5, 96) = 101

Romeo's previous value was 124.
Romeo's new value is 101.

Pushing 101 onto Juliet
Juliet's stack is now: 101 124 55 98 102 49 63

Computing int_sub(64, 4) = 60

Romeo's previous value was 101.
Romeo's new value is 60.

Pushing 60 onto Juliet
Juliet's stack is now: 60 101 124 55 98 102 49 63

Computing int_sub(64, 4) = 60

Romeo's previous value was 60.
Romeo's new value is 60.

Pushing 60 onto Juliet
Juliet's stack is now: 60 60 101 124 55 98 102 49 63

Computing int_add(32, 64) = 96

Computing int_add(8, 96) = 104

Romeo's previous value was 60.
Romeo's new value is 104.

Pushing 104 onto Juliet
Juliet's stack is now: 104 60 60 101 124 55 98 102 49 63

Computing int_sub(128, 16) = 112

Computing int_add(2, 112) = 114

Romeo's previous value was 104.
Romeo's new value is 114.

Pushing 114 onto Juliet
Juliet's stack is now: 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(2, 16) = 18

Computing int_sub(128, 18) = 110

Romeo's previous value was 114.
Romeo's new value is 110.

Pushing 110 onto Juliet
Juliet's stack is now: 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(1, 128) = 129

Romeo's previous value was 110.
Romeo's new value is 129.

Pushing 129 onto Juliet
Juliet's stack is now: 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(128, 8) = 120

Computing int_add(2, 120) = 122

Romeo's previous value was 129.
Romeo's new value is 122.

Pushing 122 onto Juliet
Juliet's stack is now: 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(4, 16) = 20

Computing int_add(1, 128) = 129

Computing int_sub(129, 20) = 109

Romeo's previous value was 122.
Romeo's new value is 109.

Pushing 109 onto Juliet
Juliet's stack is now: 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(4, 64) = 68

Romeo's previous value was 109.
Romeo's new value is 68.

Pushing 68 onto Juliet
Juliet's stack is now: 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(4, 64) = 68

Romeo's previous value was 68.
Romeo's new value is 68.

Pushing 68 onto Juliet
Juliet's stack is now: 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(128, 16) = 112

Romeo's previous value was 68.
Romeo's new value is 112.

Pushing 112 onto Juliet
Juliet's stack is now: 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(64, 1) = 63

Computing int_add(8, 63) = 71

Romeo's previous value was 112.
Romeo's new value is 71.

Pushing 71 onto Juliet
Juliet's stack is now: 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 64) = 72

Computing int_add(1, 72) = 73

Romeo's previous value was 71.
Romeo's new value is 73.

Pushing 73 onto Juliet
Juliet's stack is now: 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(2, 128) = 130

Romeo's previous value was 73.
Romeo's new value is 130.

Pushing 130 onto Juliet
Juliet's stack is now: 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 64) = 72

Computing int_add(1, 72) = 73

Romeo's previous value was 130.
Romeo's new value is 73.

Pushing 73 onto Juliet
Juliet's stack is now: 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(2, 128) = 130

Romeo's previous value was 73.
Romeo's new value is 130.

Pushing 130 onto Juliet
Juliet's stack is now: 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(64, 1) = 63

Computing int_add(8, 63) = 71

Romeo's previous value was 130.
Romeo's new value is 71.

Pushing 71 onto Juliet
Juliet's stack is now: 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 64) = 72

Computing int_add(1, 4) = 5

Computing int_add(5, 72) = 77

Romeo's previous value was 71.
Romeo's new value is 77.

Pushing 77 onto Juliet
Juliet's stack is now: 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 64) = 72

Computing int_add(4, 72) = 76

Romeo's previous value was 77.
Romeo's new value is 76.

Pushing 76 onto Juliet
Juliet's stack is now: 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(128, 8) = 120

Computing int_add(1, 120) = 121

Romeo's previous value was 76.
Romeo's new value is 121.

Pushing 121 onto Juliet
Juliet's stack is now: 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(64, 2) = 62

Computing int_add(16, 62) = 78

Romeo's previous value was 121.
Romeo's new value is 78.

Pushing 78 onto Juliet
Juliet's stack is now: 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(16, 128) = 144

Computing int_add(2, 144) = 146

Romeo's previous value was 78.
Romeo's new value is 146.

Pushing 146 onto Juliet
Juliet's stack is now: 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 64) = 72

Computing int_add(1, 4) = 5

Computing int_add(5, 72) = 77

Romeo's previous value was 146.
Romeo's new value is 77.

Pushing 77 onto Juliet
Juliet's stack is now: 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 128) = 136

Computing int_add(2, 136) = 138

Romeo's previous value was 77.
Romeo's new value is 138.

Pushing 138 onto Juliet
Juliet's stack is now: 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(128, 2) = 126

Romeo's previous value was 138.
Romeo's new value is 126.

Pushing 126 onto Juliet
Juliet's stack is now: 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(16, 128) = 144

Computing int_add(1, 4) = 5

Computing int_add(5, 144) = 149

Romeo's previous value was 126.
Romeo's new value is 149.

Pushing 149 onto Juliet
Juliet's stack is now: 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(16, 64) = 80

Computing int_add(1, 80) = 81

Romeo's previous value was 149.
Romeo's new value is 81.

Pushing 81 onto Juliet
Juliet's stack is now: 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(32, 1) = 31

Computing int_add(1, 4) = 5

Computing int_mul(5, 31) = 155

Romeo's previous value was 81.
Romeo's new value is 155.

Pushing 155 onto Juliet
Juliet's stack is now: 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(2, 128) = 130

Romeo's previous value was 155.
Romeo's new value is 130.

Pushing 130 onto Juliet
Juliet's stack is now: 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(64, 4) = 60

Computing int_sub(32, 1) = 31

Computing int_add(31, 60) = 91

Romeo's previous value was 130.
Romeo's new value is 91.

Pushing 91 onto Juliet
Juliet's stack is now: 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(16, 128) = 144

Computing int_add(1, 2) = 3

Computing int_add(3, 144) = 147

Romeo's previous value was 91.
Romeo's new value is 147.

Pushing 147 onto Juliet
Juliet's stack is now: 147 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(16, 64) = 80

Computing int_add(2, 4) = 6

Computing int_add(6, 80) = 86

Romeo's previous value was 147.
Romeo's new value is 86.

Pushing 86 onto Juliet
Juliet's stack is now: 86 147 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 128) = 136

Computing int_add(1, 2) = 3

Computing int_add(3, 136) = 139

Romeo's previous value was 86.
Romeo's new value is 139.

Pushing 139 onto Juliet
Juliet's stack is now: 139 86 147 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_add(8, 32) = 40

Romeo's previous value was 139.
Romeo's new value is 40.

Computing int_sub(40, 1) = 39

Romeo's previous value was 40.
Romeo's new value is 39.

Popping Juliet, getting the value 139
Juliet's stack is now: 86 147 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(139, 39) = 100

Juliet's previous value was 139.
Juliet's new value is 100.

Pushing 100 onto Romeo
Romeo's stack is now: 100

Computing int_sub(1, 1) = 0

Computing int_sub(39, 1) = 38

Romeo's previous value was 39.
Romeo's new value is 38.

Popping Juliet, getting the value 86
Juliet's stack is now: 147 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(86, 38) = 48

Juliet's previous value was 86.
Juliet's new value is 48.

Pushing 48 onto Romeo
Romeo's stack is now: 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(38, 1) = 37

Romeo's previous value was 38.
Romeo's new value is 37.

Popping Juliet, getting the value 147
Juliet's stack is now: 91 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(147, 37) = 110

Juliet's previous value was 147.
Juliet's new value is 110.

Pushing 110 onto Romeo
Romeo's stack is now: 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(37, 1) = 36

Romeo's previous value was 37.
Romeo's new value is 36.

Popping Juliet, getting the value 91
Juliet's stack is now: 130 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(91, 36) = 55

Juliet's previous value was 91.
Juliet's new value is 55.

Pushing 55 onto Romeo
Romeo's stack is now: 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(36, 1) = 35

Romeo's previous value was 36.
Romeo's new value is 35.

Popping Juliet, getting the value 130
Juliet's stack is now: 155 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(130, 35) = 95

Juliet's previous value was 130.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(35, 1) = 34

Romeo's previous value was 35.
Romeo's new value is 34.

Popping Juliet, getting the value 155
Juliet's stack is now: 81 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(155, 34) = 121

Juliet's previous value was 155.
Juliet's new value is 121.

Pushing 121 onto Romeo
Romeo's stack is now: 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(34, 1) = 33

Romeo's previous value was 34.
Romeo's new value is 33.

Popping Juliet, getting the value 81
Juliet's stack is now: 149 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(81, 33) = 48

Juliet's previous value was 81.
Juliet's new value is 48.

Pushing 48 onto Romeo
Romeo's stack is now: 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(33, 1) = 32

Romeo's previous value was 33.
Romeo's new value is 32.

Popping Juliet, getting the value 149
Juliet's stack is now: 126 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(149, 32) = 117

Juliet's previous value was 149.
Juliet's new value is 117.

Pushing 117 onto Romeo
Romeo's stack is now: 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(32, 1) = 31

Romeo's previous value was 32.
Romeo's new value is 31.

Popping Juliet, getting the value 126
Juliet's stack is now: 138 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(126, 31) = 95

Juliet's previous value was 126.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(31, 1) = 30

Romeo's previous value was 31.
Romeo's new value is 30.

Popping Juliet, getting the value 138
Juliet's stack is now: 77 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(138, 30) = 108

Juliet's previous value was 138.
Juliet's new value is 108.

Pushing 108 onto Romeo
Romeo's stack is now: 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(30, 1) = 29

Romeo's previous value was 30.
Romeo's new value is 29.

Popping Juliet, getting the value 77
Juliet's stack is now: 146 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(77, 29) = 48

Juliet's previous value was 77.
Juliet's new value is 48.

Pushing 48 onto Romeo
Romeo's stack is now: 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(29, 1) = 28

Romeo's previous value was 29.
Romeo's new value is 28.

Popping Juliet, getting the value 146
Juliet's stack is now: 78 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(146, 28) = 118

Juliet's previous value was 146.
Juliet's new value is 118.

Pushing 118 onto Romeo
Romeo's stack is now: 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(28, 1) = 27

Romeo's previous value was 28.
Romeo's new value is 27.

Popping Juliet, getting the value 78
Juliet's stack is now: 121 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(78, 27) = 51

Juliet's previous value was 78.
Juliet's new value is 51.

Pushing 51 onto Romeo
Romeo's stack is now: 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(27, 1) = 26

Romeo's previous value was 27.
Romeo's new value is 26.

Popping Juliet, getting the value 121
Juliet's stack is now: 76 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(121, 26) = 95

Juliet's previous value was 121.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(26, 1) = 25

Romeo's previous value was 26.
Romeo's new value is 25.

Popping Juliet, getting the value 76
Juliet's stack is now: 77 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(76, 25) = 51

Juliet's previous value was 76.
Juliet's new value is 51.

Pushing 51 onto Romeo
Romeo's stack is now: 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(25, 1) = 24

Romeo's previous value was 25.
Romeo's new value is 24.

Popping Juliet, getting the value 77
Juliet's stack is now: 71 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(77, 24) = 53

Juliet's previous value was 77.
Juliet's new value is 53.

Pushing 53 onto Romeo
Romeo's stack is now: 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(24, 1) = 23

Romeo's previous value was 24.
Romeo's new value is 23.

Popping Juliet, getting the value 71
Juliet's stack is now: 130 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(71, 23) = 48

Juliet's previous value was 71.
Juliet's new value is 48.

Pushing 48 onto Romeo
Romeo's stack is now: 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(23, 1) = 22

Romeo's previous value was 23.
Romeo's new value is 22.

Popping Juliet, getting the value 130
Juliet's stack is now: 73 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(130, 22) = 108

Juliet's previous value was 130.
Juliet's new value is 108.

Pushing 108 onto Romeo
Romeo's stack is now: 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(22, 1) = 21

Romeo's previous value was 22.
Romeo's new value is 21.

Popping Juliet, getting the value 73
Juliet's stack is now: 130 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(73, 21) = 52

Juliet's previous value was 73.
Juliet's new value is 52.

Pushing 52 onto Romeo
Romeo's stack is now: 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(21, 1) = 20

Romeo's previous value was 21.
Romeo's new value is 20.

Popping Juliet, getting the value 130
Juliet's stack is now: 73 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(130, 20) = 110

Juliet's previous value was 130.
Juliet's new value is 110.

Pushing 110 onto Romeo
Romeo's stack is now: 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(20, 1) = 19

Romeo's previous value was 20.
Romeo's new value is 19.

Popping Juliet, getting the value 73
Juliet's stack is now: 71 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(73, 19) = 54

Juliet's previous value was 73.
Juliet's new value is 54.

Pushing 54 onto Romeo
Romeo's stack is now: 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(19, 1) = 18

Romeo's previous value was 19.
Romeo's new value is 18.

Popping Juliet, getting the value 71
Juliet's stack is now: 112 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(71, 18) = 53

Juliet's previous value was 71.
Juliet's new value is 53.

Pushing 53 onto Romeo
Romeo's stack is now: 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(18, 1) = 17

Romeo's previous value was 18.
Romeo's new value is 17.

Popping Juliet, getting the value 112
Juliet's stack is now: 68 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(112, 17) = 95

Juliet's previous value was 112.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(17, 1) = 16

Romeo's previous value was 17.
Romeo's new value is 16.

Popping Juliet, getting the value 68
Juliet's stack is now: 68 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(68, 16) = 52

Juliet's previous value was 68.
Juliet's new value is 52.

Pushing 52 onto Romeo
Romeo's stack is now: 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(16, 1) = 15

Romeo's previous value was 16.
Romeo's new value is 15.

Popping Juliet, getting the value 68
Juliet's stack is now: 109 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(68, 15) = 53

Juliet's previous value was 68.
Juliet's new value is 53.

Pushing 53 onto Romeo
Romeo's stack is now: 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(15, 1) = 14

Romeo's previous value was 15.
Romeo's new value is 14.

Popping Juliet, getting the value 109
Juliet's stack is now: 122 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(109, 14) = 95

Juliet's previous value was 109.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(14, 1) = 13

Romeo's previous value was 14.
Romeo's new value is 13.

Popping Juliet, getting the value 122
Juliet's stack is now: 129 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(122, 13) = 109

Juliet's previous value was 122.
Juliet's new value is 109.

Pushing 109 onto Romeo
Romeo's stack is now: 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(13, 1) = 12

Romeo's previous value was 13.
Romeo's new value is 12.

Popping Juliet, getting the value 129
Juliet's stack is now: 110 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(129, 12) = 117

Juliet's previous value was 129.
Juliet's new value is 117.

Pushing 117 onto Romeo
Romeo's stack is now: 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(12, 1) = 11

Romeo's previous value was 12.
Romeo's new value is 11.

Popping Juliet, getting the value 110
Juliet's stack is now: 114 104 60 60 101 124 55 98 102 49 63

Computing int_sub(110, 11) = 99

Juliet's previous value was 110.
Juliet's new value is 99.

Pushing 99 onto Romeo
Romeo's stack is now: 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(11, 1) = 10

Romeo's previous value was 11.
Romeo's new value is 10.

Popping Juliet, getting the value 114
Juliet's stack is now: 104 60 60 101 124 55 98 102 49 63

Computing int_sub(114, 10) = 104

Juliet's previous value was 114.
Juliet's new value is 104.

Pushing 104 onto Romeo
Romeo's stack is now: 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(10, 1) = 9

Romeo's previous value was 10.
Romeo's new value is 9.

Popping Juliet, getting the value 104
Juliet's stack is now: 60 60 101 124 55 98 102 49 63

Computing int_sub(104, 9) = 95

Juliet's previous value was 104.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(9, 1) = 8

Romeo's previous value was 9.
Romeo's new value is 8.

Popping Juliet, getting the value 60
Juliet's stack is now: 60 101 124 55 98 102 49 63

Computing int_sub(60, 8) = 52

Juliet's previous value was 60.
Juliet's new value is 52.

Pushing 52 onto Romeo
Romeo's stack is now: 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(8, 1) = 7

Romeo's previous value was 8.
Romeo's new value is 7.

Popping Juliet, getting the value 60
Juliet's stack is now: 101 124 55 98 102 49 63

Computing int_sub(60, 7) = 53

Juliet's previous value was 60.
Juliet's new value is 53.

Pushing 53 onto Romeo
Romeo's stack is now: 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(7, 1) = 6

Romeo's previous value was 7.
Romeo's new value is 6.

Popping Juliet, getting the value 101
Juliet's stack is now: 124 55 98 102 49 63

Computing int_sub(101, 6) = 95

Juliet's previous value was 101.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(6, 1) = 5

Romeo's previous value was 6.
Romeo's new value is 5.

Popping Juliet, getting the value 124
Juliet's stack is now: 55 98 102 49 63

Computing int_sub(124, 5) = 119

Juliet's previous value was 124.
Juliet's new value is 119.

Pushing 119 onto Romeo
Romeo's stack is now: 119 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(5, 1) = 4

Romeo's previous value was 5.
Romeo's new value is 4.

Popping Juliet, getting the value 55
Juliet's stack is now: 98 102 49 63

Computing int_sub(55, 4) = 51

Juliet's previous value was 55.
Juliet's new value is 51.

Pushing 51 onto Romeo
Romeo's stack is now: 51 119 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(4, 1) = 3

Romeo's previous value was 4.
Romeo's new value is 3.

Popping Juliet, getting the value 98
Juliet's stack is now: 102 49 63

Computing int_sub(98, 3) = 95

Juliet's previous value was 98.
Juliet's new value is 95.

Pushing 95 onto Romeo
Romeo's stack is now: 95 51 119 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(3, 1) = 2

Romeo's previous value was 3.
Romeo's new value is 2.

Popping Juliet, getting the value 102
Juliet's stack is now: 49 63

Computing int_sub(102, 2) = 100

Juliet's previous value was 102.
Juliet's new value is 100.

Pushing 100 onto Romeo
Romeo's stack is now: 100 95 51 119 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(2, 1) = 1

Romeo's previous value was 2.
Romeo's new value is 1.

Popping Juliet, getting the value 49
Juliet's stack is now: 63

Computing int_sub(49, 1) = 48

Juliet's previous value was 49.
Juliet's new value is 48.

Pushing 48 onto Romeo
Romeo's stack is now: 48 100 95 51 119 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Computing int_sub(1, 1) = 0

Romeo's previous value was 1.
Romeo's new value is 0.

Popping Juliet, getting the value 63
Juliet's stack is now:

Computing int_sub(63, 0) = 63

Juliet's previous value was 63.
Juliet's new value is 63.

Pushing 63 onto Romeo
Romeo's stack is now: 63 48 100 95 51 119 95 53 52 95 104 99 117 109 95 53 52 95 53 54 110 52 108 48 53 51 95 51 118 48 108 95 117 48 121 95 55 110 48 100

Computing int_sub(1, 1) = 0

Everybody just exeunted
               Romeo
              Juliet
